package com.nCubes.Attendo;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.nCubes.Attendo.Glearn.FileDownloader;
import com.nCubes.Attendo.Glearn.Grades;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by Toship on 19-09-2017.
 */

public class ExpandableAdapter extends BaseExpandableListAdapter {

    private LinkedHashMap<String, LinkedHashMap<String, String>> resources;
    private ArrayList<Grades> grades;
    private Context context;
    private static Map<String, String> cookie;
    private String CourseName;
    private float attainper;

    ExpandableAdapter(Context context,
                             LinkedHashMap<String, LinkedHashMap<String, String>> resources,
                             Map<String, String> cookie, String CourseName, float attainper) {
        this.context = context;
        this.resources = resources;
        ExpandableAdapter.cookie = cookie;
        this.CourseName = CourseName;
        this.attainper = attainper;
    }

    @Override
    public int getGroupCount() {
        return resources.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        String pos = (new ArrayList<>(resources.keySet())).get(groupPosition);
        resources.get(pos).remove("Course Objectives Placeholder File.pdf");
        resources.get(pos).remove("Course Syllabus Placeholder File.pdf");
        resources.get(pos).remove("Course Teachingplan Placeholder File.pdf");
        resources.get(pos).remove("Course Content Placeholder File.pdf");
        return resources.get(pos).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return (new ArrayList<>(resources.keySet())).get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        String pos = (new ArrayList<>(resources.keySet())).get(groupPosition);
        return (new ArrayList<>(resources.get(pos).keySet())).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        final String groupText = (String) getGroup(groupPosition);

        if (convertView == null) {
             this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = View.inflate(context ,R.layout.list_group, null);
        }

        TextView textView = convertView.findViewById(R.id.group_item);
        textView.setText(groupText);

        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        if (convertView == null) {
            this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = View.inflate(context, R.layout.list_child, null);
        }

        final String childText = (String) getChild(groupPosition, childPosition);

        TextView textView = convertView.findViewById(R.id.child_item);
        ImageView contentImg = convertView.findViewById(R.id.content_img);

        String pos = (new ArrayList<>(resources.keySet())).get(groupPosition);
        final String url = resources.get(pos).get(childText);

        textView.setText(childText);
        switch ((Pattern.compile("\\/folder\\/").matcher(url).find()) ? 1 : 0) {
            case 1:
                contentImg.setImageResource(R.drawable.ic_folder);
                break;
            case 0:
                contentImg.setImageResource(R.drawable.ic_file);
                break;
        }

        ((View) textView.getParent()).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isConnected()) {
                    Toast.makeText(context, "Loading...", Toast.LENGTH_SHORT).show();
                    FileDownloader fileDownloader = new FileDownloader(view.getContext(), childText, url,
                            cookie, CourseName, attainper);
                    fileDownloader.execute();
                } else {
                    Toast.makeText(context, "No Internet Connection!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    /**
     * Internet Connection Status
     */
    private boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }
}
