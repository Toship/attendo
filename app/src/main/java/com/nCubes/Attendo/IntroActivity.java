package com.nCubes.Attendo;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.heinrichreimersoftware.materialintro.slide.Slide;

public class IntroActivity extends com.heinrichreimersoftware.materialintro.app.IntroActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addSlide(getSlide("We care about\nyour attendance.", "Attendo Fulfils all your needs.", R.drawable.login_page));
        addSlide(getSlide("We want you\nto be perfect.", "Track your attendance everyday.", R.drawable.main_page));
        addSlide(getSlide("We can help\npredict your future.", "Predict your attendance upto 10 classes.", R.drawable.predictions_page));
        addSlide(getSlide("We believe in you.", "View your attendance\nstatistics for each course.", R.drawable.course_page));
        addSlide(getSlide("We care for\nyour needs.", "Download all course materials", R.drawable.downloads_page));
        addSlide(getSlide("...In the end, it's all about you!", "View your grades for a semester.", R.drawable.grades_page));

    }

    Slide getSlide(final String title, final String des, final int img) {
        return new Slide() {
            @Override
            public Fragment getFragment() {
                return TutorialFragment.newInstance(title, des, img);
            }

            @Override
            public int getBackground() {
                return R.color.bck_blue;
            }

            @Override
            public int getBackgroundDark() {
                return R.color.blue_darker;
            }

            @Override
            public boolean canGoForward() {
                return true;
            }

            @Override
            public boolean canGoBackward() {
                return true;
            }
        };
    }

}
