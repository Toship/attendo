package com.nCubes.Attendo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import com.jackandphantom.circularprogressbar.CircleProgressbar;
import com.nCubes.Attendo.Glearn.Attendance;
import com.nCubes.Attendo.Glearn.Course;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

public class Classroom extends AppCompatActivity {

    private Course course = null;
    private Map<String, String> cookie = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classroom);

        //Set The Ads

        Bundle bundle = getIntent().getExtras();
        course = (Course) bundle.getSerializable("course");
        cookie = (Map<String, String>) bundle.getSerializable("cookie");
        float avgper = course.getPerAtten();

        //Setting CircularProgressbar
        CircleProgressbar subProgress = findViewById(R.id.cpb);
        TextView subAtten = findViewById(R.id.sub_progress);
        TextView toolText = findViewById(R.id.textView);
        DecimalFormat df = new DecimalFormat("#.#");
        subAtten.setText(df.format(avgper) + "%");
        subProgress.setProgressWithAnimation((int) avgper, 2500);
        subProgress.enabledTouch(false);

        //Setting Prediction texts
        TextView pos = findViewById(R.id.atten_plus);
        TextView neg = findViewById(R.id.atten_minus);
        pos.setText(getIntent().getStringExtra("pos"));
        neg.setText(getIntent().getStringExtra("neg"));

        //Setting everyday Attendance List
        RecyclerView recyclerView = findViewById(R.id.classroom_recycle_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        ArrayList<Attendance> attendances = course.getAttendance();
        Collections.reverse(attendances);
        AttendanceAdapter adapter = new AttendanceAdapter(3, attendances);
        recyclerView.setAdapter(adapter);
        toolText.setText(course.getName());

        //Setting Back OnClickListener
        backOnClick();

        //Setting Course Material OnClickListener
        courseMaterialOnClick();

        //Setting Popup Button OnClickListener
        //Setting Popup
        setPopUpButton();

        setTheme();

    }

    void backOnClick(){
        ImageView backButton = findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        });
    }

    void courseMaterialOnClick(){
        LinearLayout courseMaterial = findViewById(R.id.course_materials);
        courseMaterial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Classroom.this, CourseContent.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("resource", course.getResources());
                bundle.putSerializable("cookie", (Serializable) cookie);
                intent.putExtra("attain", course.getPerAtten());
                intent.putExtra("name", course.getName());
                intent.putExtras(bundle);
                view.getContext().startActivity(intent);
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            }
        });
    }

    void setPopUpButton(){
        final ImageView popupButton = findViewById(R.id.popup);
        popupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(Classroom.this, popupButton);
                popupMenu.getMenuInflater().inflate(R.menu.overflow, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        if(menuItem.getTitle().equals("Grades")){
                            Intent intent = new Intent(Classroom.this, GradesActivity.class);
                            startActivity(intent);
                            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                        } else if(menuItem.getTitle().equals("Logout")){
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                android.webkit.CookieManager.getInstance().removeAllCookies(null);
                            }
                            else {
                                android.webkit.CookieManager.getInstance().removeAllCookie();
                            }
                            Intent intent = new Intent(Classroom.this, LoginActivity.class);
                            intent.putExtra("Logout", 1);
                            finish();
                            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                            startActivity(intent);
                            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                        } else {
                            SharedPreferences.Editor editor = getSharedPreferences("Theme", MODE_PRIVATE).edit();
                            if (menuItem.getTitle().equals("Aubergine")) {
                                editor.putInt("Color1", -5634197);
                                editor.putInt("Color2", -10419105);
                            } else if (menuItem.getTitle().equals("Cherry")) {
                                editor.putInt("Color1", -1363127);
                                editor.putInt("Color2", -762813);
                            } else if (menuItem.getTitle().equals("Mango Pulp")) {
                                editor.putInt("Color1", -1009639);
                                editor.putInt("Color2", -1188259);
                            } else if (menuItem.getTitle().equals("Green Beach")) {
                                editor.putInt("Color1", -16602448);
                                editor.putInt("Color2", -16724564);
                            } else if (menuItem.getTitle().equals("Mantle")) {
                                editor.putInt("Color1", -14367012);
                                editor.putInt("Color2", -11449699);
                            }
                            editor.apply();
                            setTheme();
                        }
                        return true;
                    }
                });
                popupMenu.show();
            }
        });
    }

    void setTheme(){
        SharedPreferences preferences = getSharedPreferences("Theme", MODE_PRIVATE);
        LinearLayout mainLayout = findViewById(R.id.main_layout);
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM,
                new int[] {preferences.getInt("Color1", -14367012), preferences.getInt("Color2", -11449699)});
        gd.setCornerRadius(0f);
        mainLayout.setBackground(gd);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        setTheme();
    }
}
