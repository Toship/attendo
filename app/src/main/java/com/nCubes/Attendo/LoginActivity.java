package com.nCubes.Attendo;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.nCubes.Attendo.Glearn.Course;
import com.nCubes.Attendo.Glearn.CourseRetriver;
import com.victor.loading.rotate.RotateLoading;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;


/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    private static final String domain = "https://glearn.gitam.edu";
    private final String loginUrl = "https://glearn.gitam.edu/login/index.php";
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;
    private ArrayList<Course> courses = new ArrayList<>();
    // UI references.
    private RotateLoading mProgressView;
    private WebView webView;
    private Bitmap bmp;
    private Button cancel,reload;
    private FloatingActionButton gradesLink;
    private RelativeLayout web_progress;
    private boolean stop = false;
    private ImageView fire_img;
    private String cookies = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setTips();

        // Set up the login form.
        fire_img = findViewById(R.id.firebase_img);
        mProgressView = findViewById(R.id.login_progress);
        gradesLink = findViewById(R.id.grades_link);
        web_progress = findViewById(R.id.web_view_progress);
        web_progress.setVisibility(View.VISIBLE);
        webView = findViewById(R.id.login_form);
        cancel = findViewById(R.id.cancel_button);
        reload = findViewById(R.id.reload_button);
        TextView loginHeader = findViewById(R.id.login_page_app_name);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Milkshake.ttf");
        loginHeader.setTypeface(typeface);
        gradesLink.bringToFront();
        if (getIntent().getIntExtra("Logout", 0) == 1) {
            System.out.println("Logout");
            CookieManager.getInstance().removeAllCookie();
            webView.clearHistory();
            webView.clearFormData();
            webView.clearCache(true);
            webView.clearMatches();
            webView.clearSslPreferences();
            webView.loadUrl(loginUrl);
        } else {
            webView.loadUrl(domain);
        }

        mProgressView.start();
        //Setting OnClickListener on Grades Button
        gradesLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, GradesActivity.class);
                startActivity(intent);
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            }
        });

        //Setting OnclickListener on Reload button
        reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println(cookies);
                if(cookies != null)
                    webView.loadUrl("https://glearn.gitam.edu/my/");
                else
                    webView.reload();
            }
        });

        //Setting OnClickListener on Cancel button
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAuthTask.cancel(true);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    CookieManager.getInstance().removeAllCookies(null);
                }
                else {
                    CookieManager.getInstance().removeAllCookie();
                }
                cookies = null;
                webView.reload();
            }
        });

        setTheme();
        CheckUpdate();

    }

    private void setAdImage(){
        //Setting Image
        StorageReference storageReference = FirebaseStorage.getInstance().getReference();
        final StorageReference imgRef = storageReference.child("image.png");
        final long ONE_MEGABYTE = 1024 * 1024;
        imgRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                 bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                fire_img.setImageBitmap(bmp);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                fire_img.setVisibility(View.GONE);
            }
        });

        //Setting Link on Image
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference linkref = database.getReference("link");
        linkref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final String link = dataSnapshot.getValue(String.class);
               // if (link == null || link.equals("NO")) {
                    fire_img.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                                try {
                                    if(bmp!=null) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        intent.setData(Uri.parse(link));
                                        startActivity(intent);
                                    }
                                    else{
                                        Log.e("Ad Report","Bitmap Resource is NULL.");
                                    }
                                } catch (Exception e){
                                    //Nothing happens
                                    Log.e("Ad report","Image true link false");
                                }
                            }
                    });
                //}
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void setTips(){
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                //  Initialize SharedPreferences
                SharedPreferences getPrefs = PreferenceManager
                        .getDefaultSharedPreferences(getBaseContext());

                //  Create a new boolean and preference and set it to true
                boolean isFirstStart = getPrefs.getBoolean("firstStart", true);

                //  If the activity has never started before...
                if (isFirstStart) {

                    //  Launch app intro
                    final Intent i = new Intent(LoginActivity.this, IntroActivity.class);

                    runOnUiThread(new Runnable() {
                        @Override public void run() {
                            startActivity(i);
                        }
                    });

                    //  Make a new preferences editor
                    SharedPreferences.Editor e = getPrefs.edit();

                    //  Edit preference to make it false because we don't want this to run again
                    e.putBoolean("firstStart", false);

                    //  Apply changes
                    e.apply();
                }
            }
        });

        // Start the thread
        t.start();
    }

    /**
     * Check for Updates.
     * But only once in a day.
     */
    private void CheckUpdate(){
        SharedPreferences preferences = getSharedPreferences("Date", MODE_PRIVATE);
        String date = preferences.getString("lastDate", "0");
        String currentDate = DateFormat.getDateInstance().format(new Date());
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            if(!date.equals(currentDate)){
                new CheckUpdates(version).execute();
                SharedPreferences.Editor editor = getSharedPreferences("Date", MODE_PRIVATE).edit();
                editor.putString("lastDate", currentDate);
                editor.apply();
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    void setTheme(){
        SharedPreferences preferences = getSharedPreferences("Theme", MODE_PRIVATE);
        LinearLayout mainLayout = findViewById(R.id.main_linear);
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM,
                new int[] {preferences.getInt("Color1", -14367012), preferences.getInt("Color2", -11449699)});
        gd.setCornerRadius(0f);
        mainLayout.setBackground(gd);
    }

    /**
     * Webview setup
     */
    @SuppressLint("SetJavaScriptEnabled")
    private void setWebView(){
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                //System.out.println("onPageStarted"+url);
                web_progress.setVisibility(View.VISIBLE);
                if (url.equals("https://glearn.gitam.edu/my/")) {
                    view.stopLoading();
                    showProgress(true);
                    cookies = CookieManager.getInstance().getCookie(url);
                    Log.e("MA", cookies);
                    mAuthTask = new UserLoginTask(cookies);
                    mAuthTask.execute();
                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                //System.out.println("onPageFinished" + url);
                web_progress.setVisibility(View.GONE);
                if (url.equals(loginUrl)) {
                    view.loadUrl("javascript:(function() { " +
                            "document.getElementsByClassName('navbar navbar-fixed-top moodle-has-zindex')[0].style.display='none'; " +
                            "document.getElementsByClassName('container-fluid')[0].style.display='none'; " +
                            "document.getElementById('page-footer').style.display='none'; " +
                            "document.getElementsByClassName('form-label')[2].style.display='none';" +
                            "document.getElementsByClassName('forgetpass')[0].style.display='none'; " +
                            "document.getElementsByClassName('desc')[0].style.display='none'; " +
                            "}" +
                            ")()");
                    view.scrollBy(200, 0);
                } else if (url.equals("https://glearn.gitam.edu/my/")){
                    showProgress(true);
                } else if (url.equals("data:text/html,chromewebdata")){
                    Toast.makeText(LoginActivity.this, "No Internet Connection!", Toast.LENGTH_SHORT).show();
                } else {
                    //System.out.println("url: "+url);
                    view.loadUrl(loginUrl);
                    Toast.makeText(LoginActivity.this, "Stay on the Login page!",
                            Toast.LENGTH_SHORT).show();
                }
                setAdImage();
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        finish();
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    private void showProgress(final boolean show) {

        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        webView.setVisibility(show ? View.GONE : View.VISIBLE);
        webView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                webView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        gradesLink.setVisibility(show ? View.GONE : View.VISIBLE);
        reload.setVisibility(show ? View.GONE : View.VISIBLE);
        cancel.setVisibility(show ? View.VISIBLE : View.GONE);
        fire_img.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);

        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });

        FrameLayout frameLayout = findViewById(R.id.login_frame_layout);
        frameLayout.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    @Override
    protected void onStop() {
        stop = true;
        super.onStop();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        stop = false;
        if(cookies == null) {
            showProgress(false);
            setWebView();
        } else {
            showProgress(true);
            reload.performClick();
        }
    }

    /**
     * Async class to check for update.
     */
    private class CheckUpdates extends AsyncTask<Void, Void, Integer> {

        private String appURL = "https://play.google.com/store/apps/details?id=com.nCubes.Attendo&hl=en";
        private String versionName;
        private String currentversion;

        CheckUpdates(String currentversion){
            this.currentversion = currentversion;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            try {
                Document page = Jsoup.connect(appURL).get();
                versionName = page.select("div[itemprop=softwareVersion]").text();
                return 0;
            } catch (IOException e) {
                e.printStackTrace();
                return 1;
            }
        }

        @Override
        protected void onPostExecute(Integer error) {
            if(error == 0 && !currentversion.equals(versionName)){
                AlertDialog.Builder alert = new AlertDialog.Builder(LoginActivity.this);
                alert.setIcon(R.drawable.attendo_logo).setTitle("An Update Is Available!")
                        .setMessage("Do You Want To Update The App?")
                        .setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setPositiveButton("Update now", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                try {
                                    startActivity(new Intent(Intent.ACTION_VIEW,
                                            Uri.parse("market://details?id=com.nCubes.Attendo")));
                                    overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                                }
                                catch (android.content.ActivityNotFoundException anfe) {
                                    startActivity(new Intent(Intent.ACTION_VIEW,
                                            Uri.parse("http://play.google.com/store/apps/details?id=com.nCubes.Attendo")));
                                    overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                                }
                            }
                        }).show();
            }
        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    private class UserLoginTask extends AsyncTask<Void, Void, Integer> {

        private Document homepage;
        private Map<String, String> cookie = new HashMap<>();
        private ThreadPoolExecutor executor;
        private ArrayList<CourseRetriver> courseRetrivers = new ArrayList<>();
        private String image;
        private String Sname;

        UserLoginTask(String Cookie) {
            String[] cookiesplit = Cookie.split("\\s");
            for (String a: cookiesplit){
                this.cookie.put(a.split("=")[0], a.split("=")[1]);
            }
        }

        private void hitCourseLinks() {
            executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(4);
            try {
                homepage = Jsoup.connect("https://glearn.gitam.edu/my/").cookies(cookie).get();
                Elements els = homepage.select("div[class=visible-desktop]");
                for (Element link : els) {
                    String links = link.select("a[href]").attr("href");
                    //System.out.println(links);
                    CourseRetriver courseRetriver = new CourseRetriver(links, cookie);
                    Runnable runnable = courseRetriver;
                    executor.execute(runnable);
                    courseRetriver = (CourseRetriver) runnable;
                    courseRetrivers.add(courseRetriver);
                }
                executor.shutdown();
                while (!executor.isTerminated()) {
                    if (stop) {
                        executor.shutdownNow();
                        return;
                    }
                    Thread.sleep(1000);
                }
                image = homepage.select("header[id=page-header]").select("img[src]").attr("src");
                String[] name = homepage.select("h1").text().split(", ");
                Sname = name[1] + "\n" + name[0];
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        private int collectCourses() throws Exception {
            for (CourseRetriver courseRetriver : courseRetrivers) {
                if (courseRetriver.getCourse() != null) {
                    courses.add(courseRetriver.getCourse());
                } else if (stop){
                    return 1;
                } else {
                    Log.e("collectCourse", "course is null");
                    return 2;
                }
            }
            return 0;
        }

        @Override
        protected Integer doInBackground(Void... params) {

            try {
                hitCourseLinks();
                if (stop) {
                    return 1;
                }
                return collectCourses();

            } catch (IOException e) {
                Log.e("LoginActivity", "IOException", e);
                return 2;
            } catch (Exception e) {
                Log.e("LoginActivity", "NullPointException", e);
                return 2;
            }

        }

        @Override
        protected void onPostExecute(Integer status) {
            mAuthTask = null;

            try{
                System.out.println(homepage.select("h1").text());
            } catch (NullPointerException e){
                status = 2;
            }

            if (status == 0) {
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("ArrayList", courses);
                bundle.putSerializable("cookie", (Serializable) cookie);
                intent.putExtras(bundle);
                intent.putExtra("image", image);
                intent.putExtra("Sname", Sname);
                startActivity(intent);
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            } else if (status == 2) {
                AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
                alertDialog.setTitle("Connection Error!");
                alertDialog.setMessage("Slow Internet Connection!");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Intent intent = getIntent();
                                intent.putExtra("Logout", 1);
                                finish();
                                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                                startActivity(intent);
                                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                            }
                        });
                try {
                    alertDialog.show();
                } catch (Exception e) {
                    Toast.makeText(LoginActivity.this, "Slow Internet Connection!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
            showProgress(false);
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
}

