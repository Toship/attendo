package com.nCubes.Attendo;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nCubes.Attendo.Glearn.Resource;

import java.util.Map;

public class CourseContent extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_content);

        //Setting Back Button OnClickListener
        backOnClick();

        //Getting Disk writing permission
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        if (!isStoragePermissionGranted()) {
            finish();
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }

        //Setting Toolbar Name
        String name = getIntent().getStringExtra("name");
        TextView toolText = findViewById(R.id.textView);
        toolText.setText(name);

        //Setting Expandable List View
        Bundle bundle = getIntent().getExtras();
        float attainPer = getIntent().getFloatExtra("attain", 0);
        Resource resources = (Resource) bundle.getSerializable("resource");
        Map<String, String> cookie = (Map<String, String>) bundle.getSerializable("cookie");
        ExpandableListView expandableListView = findViewById(R.id.expandableListView);
        ExpandableAdapter adapter = new ExpandableAdapter(this, resources.getResource(),
                cookie, name, attainPer);
        expandableListView.setAdapter(adapter);
        for (int i = 0; i < adapter.getGroupCount(); i++) {
            expandableListView.expandGroup(i);
        }

        //Setting Popup button
        popupSetup();

        setTheme();

    }

    void backOnClick(){
        ImageView backButton = findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        });
    }

    void popupSetup(){
        final ImageView popupButton = findViewById(R.id.popup);
        popupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(CourseContent.this, popupButton);
                popupMenu.getMenuInflater().inflate(R.menu.overflow, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        if(menuItem.getTitle().equals("Grades")){
                            Intent intent = new Intent(CourseContent.this, GradesActivity.class);
                            startActivity(intent);
                            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                        } else if(menuItem.getTitle().equals("Logout")){
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                android.webkit.CookieManager.getInstance().removeAllCookies(null);
                            }
                            else {
                                android.webkit.CookieManager.getInstance().removeAllCookie();
                            }
                            Intent intent = new Intent(CourseContent.this, LoginActivity.class);
                            intent.putExtra("Logout", 1);
                            finish();
                            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                            startActivity(intent);
                            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                        } else {
                            SharedPreferences.Editor editor = getSharedPreferences("Theme", MODE_PRIVATE).edit();
                            if (menuItem.getTitle().equals("Aubergine")) {
                                editor.putInt("Color1", -5634197);
                                editor.putInt("Color2", -10419105);
                            } else if (menuItem.getTitle().equals("Cherry")) {
                                editor.putInt("Color1", -1363127);
                                editor.putInt("Color2", -762813);
                            } else if (menuItem.getTitle().equals("Mango Pulp")) {
                                editor.putInt("Color1", -1009639);
                                editor.putInt("Color2", -1188259);
                            } else if (menuItem.getTitle().equals("Green Beach")) {
                                editor.putInt("Color1", -16602448);
                                editor.putInt("Color2", -16724564);
                            } else if (menuItem.getTitle().equals("Mantle")) {
                                editor.putInt("Color1", -14367012);
                                editor.putInt("Color2", -11449699);
                            }
                            editor.apply();
                            setTheme();
                        }
                        return true;
                    }
                });
                popupMenu.show();
            }
        });
    }

    void setTheme(){
        SharedPreferences preferences = getSharedPreferences("Theme", MODE_PRIVATE);
        RelativeLayout mainLayout = findViewById(R.id.main_layout_course_content);
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM,
                new int[] {preferences.getInt("Color1", -14367012), preferences.getInt("Color2", -11449699)});
        gd.setCornerRadius(0f);
        mainLayout.setBackground(gd);
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("CC", "Permission is granted");
                return true;
            } else {

                Log.v("CC", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("CC", "Permission is granted");
            return true;
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        setTheme();
    }
}
