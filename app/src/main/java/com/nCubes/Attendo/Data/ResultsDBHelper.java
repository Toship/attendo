package com.nCubes.Attendo.Data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.nCubes.Attendo.Data.AttendoContract.Result;

/**
 * Created by Toship on 18-10-2017.
 */

public class ResultsDBHelper extends SQLiteOpenHelper {

    private final static String DATABASE_NAME = "AttendoData.db";
    private final static int DATABASE_VERSION = 1;

    public ResultsDBHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        String Create_Table_Results = "CREATE TABLE " + Result.TABLE_NAME + " ("
                + Result._ID + " TEXT PRIMARY KEY, "
                + Result.RESULTS_SEM + " TEXT, "
                + Result.RESULTS_SGPA + " REAL NOT NULL, "
                + Result.RESULTS_CGPA + " REAL NOT NULL);";

        sqLiteDatabase.execSQL(Create_Table_Results);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
