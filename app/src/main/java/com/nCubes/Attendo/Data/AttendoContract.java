package com.nCubes.Attendo.Data;

import android.provider.BaseColumns;

/**
 * Created by Toship on 18-10-2017.
 */

public final class AttendoContract  {

    public AttendoContract() { }

    public final static class Result implements BaseColumns {

        public final static String TABLE_NAME = "results";
        public final static String _ID = BaseColumns._ID;
        public final static String RESULTS_SEM = "sem";
        public final static String RESULTS_SGPA = "sgpa";
        public final static String RESULTS_CGPA = "cgpa";

    }

}
