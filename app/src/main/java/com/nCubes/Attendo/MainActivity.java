package com.nCubes.Attendo;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.jackandphantom.circularprogressbar.CircleProgressbar;
import com.nCubes.Attendo.Glearn.Course;
import java.io.IOException;
import java.lang.reflect.Array;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private static Map<String, String> cookie = null;
    private String propic;
    private CircleProgressbar mainProgress;
    private TextView mainAtten;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        //Getting Data from LoginActivity
        final Bundle bundle = getIntent().getExtras();
        final ArrayList<Course> courses = (ArrayList<Course>) bundle.getSerializable("ArrayList");
        cookie = (Map<String, String>) bundle.getSerializable("cookie");
        propic = getIntent().getStringExtra("image");
        String Sname = getIntent().getStringExtra("Sname");

        //Setting name Toolbar
        TextView textView = findViewById(R.id.textView);
        textView.setText(Sname);

        mainProgress = findViewById(R.id.main_progress);
        mainAtten = findViewById(R.id.atten_current);
        float classTaken = 0, totalClass = 0;
        final float avgper;
        try {
            for (int i = 0; i < courses.size() - 1; i++)
                for (int j = i + 1; j < courses.size(); j++)
                    if (courses.get(i).getCourseId().equals(courses.get(j).getCourseId()))
                        if (courses.get(j).getPerAtten() > courses.get(i).getPerAtten())
                            courses.remove(i);
                        else
                            courses.remove(j);

            for(Course course : courses) {
                classTaken += course.getClassTaken();
                totalClass += course.getTotalClass();
            }

        } catch (NullPointerException e){
            Toast.makeText(this, "Data did't load properly.\nTrying agian!", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent);
            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            finish();
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            e.printStackTrace();
        }
        if(totalClass == 0){
            totalClass = 1;
        }
        avgper = classTaken * 100 / totalClass;
        final DecimalFormat df = new DecimalFormat("#.##");
        mainAtten.setText(df.format(avgper) + "%");
        mainProgress.setProgressWithAnimation((int) avgper, 2500);
        mainProgress.enabledTouch(false);

        //Downloading and setting Profile pic on toolbar
        setImage setImage = new setImage();
        setImage.execute();

        //Setting Course Horizontal RecycleView
        Integer colorOrder[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        Collections.shuffle(Arrays.asList(colorOrder));
        AttendanceAdapter adapter = new AttendanceAdapter(courses, 1,
                cookie, colorOrder, MainActivity.this);
        RecyclerView recycledView = findViewById(R.id.recycle_view);
        recycledView.setAdapter(adapter);


        //Selective Attendance
        final String[] courseName = new String[courses.size()];
        final Boolean[] status = new Boolean[courses.size()];
        for (int i=0; i<courses.size(); i++){
            courseName[i] = courses.get(i).getName();
            status[i] = true;
        }
        final ImageView sort = findViewById(R.id.sort_icon);
        //Setting OnClickListener on Selective Attendance
        sort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                // Set the dialog title
                builder.setView(View.inflate(MainActivity.this,
                            R.layout.filter_layout, null)).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        float dclasstaken = 0, dtotalclass = 0;
                                int courseCount = 0;
                                for (int j=0; j<courses.size(); j++){
                                    if(status[j]) {
                                        dclasstaken += courses.get(j).getClassTaken();
                                        dtotalclass += courses.get(j).getTotalClass();
                                        courseCount += 1;
                                    }
                                }
                                final float davg = dclasstaken * 100 / dtotalclass;
                                mainAtten.setText(df.format(davg) + "%");
                                mainProgress.setProgressWithAnimation((int) davg, 2500);
                                if(courseCount != courses.size()){
                                    sort.setBackgroundColor(Color.parseColor("#116cff"));
                                    Toast.makeText(MainActivity.this,"Filter is enabled!",Toast.LENGTH_SHORT).show();
                                } else {
                                    sort.setBackgroundColor(Color.parseColor("#ffffff"));
                                }
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                RecyclerView filterAttenRecycle = alertDialog.findViewById(R.id.filter_recycle_view);
                filterAttenRecycle.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                AttendanceAdapter filterAdapter = new AttendanceAdapter(courseName, status, 2);
                filterAttenRecycle.setAdapter(filterAdapter);
            }
        });


        //Predict attendance
        ImageView predict = findViewById(R.id.predict_icon);
        final float finalClassTaken = classTaken;
        final float finalTotalClass = totalClass;
        //Setting OnClickListener on prediction Button
        predict.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setView(View.inflate(MainActivity.this, R.layout.predict, null));

                final String[] classes = new String[10];
                for(Integer i=1; i<11; i++)
                    classes[i-1] = i.toString();

                builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();

                final float[] Pclasstaken = {0};
                final float[] ptotalclass = {0};
                final Spinner total = alertDialog.findViewById(R.id.total_spinner);
                final ArrayAdapter<String> adapter1 = new ArrayAdapter<>(MainActivity.this,
                        android.R.layout.simple_spinner_dropdown_item, classes);
                adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                total.setAdapter(adapter1);
                final Spinner attend = alertDialog.findViewById(R.id.attend_spinner);

                //Setting OnItemSelectedListener on Total classes spinner
                total.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        ptotalclass[0] = i+1;
                        final String[] taken = new String[i+2];
                        for (Integer k=0; k<=ptotalclass[0]; k++)
                            taken[k] = k.toString();
                        final ArrayAdapter<String> adapter2 = new ArrayAdapter<>(MainActivity.this,
                                android.R.layout.simple_spinner_dropdown_item, taken);
                        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        attend.setAdapter(adapter2);

                        attend.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                Pclasstaken[0] = i+1;
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {
                                Pclasstaken[0] = 0;
                            }
                        });
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                        ptotalclass[0] = 0;
                    }
                });

                //Setting OnClickListener on Calculate
                Button calculate = alertDialog.findViewById(R.id.calculate);
                final TextView result = alertDialog.findViewById(R.id.result_prediction);
                calculate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(ptotalclass[0] == 0){
                            Toast.makeText(MainActivity.this, "Select Total Classes!", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        float avg = (finalClassTaken+Pclasstaken[0])*100/(finalTotalClass+ptotalclass[0]);
                        avg = Math.round(avg*100.0)/100.0f;
                        if(avg >= avgper){
                            result.setTextColor(Color.parseColor("#00ff00"));
                            if(avgper > 75){
                                Toast.makeText(MainActivity.this, "You're a Good Student!", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(MainActivity.this, "Keep going!\nYou're doing great!", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            result.setTextColor(Color.parseColor("#ff0000"));
                            if(avgper > 75){
                                Toast.makeText(MainActivity.this, "Its okay!\nI believe in you!", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(MainActivity.this, "Please Go To Class!!", Toast.LENGTH_SHORT).show();
                            }
                        }
                        result.setText(avg + "%");
                    }
                });
            }
        });

        setupPopup();
        setTheme();
    }

    void setupPopup(){
        final ImageView popupButton = findViewById(R.id.popup);
        popupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(MainActivity.this, popupButton);
                popupMenu.getMenuInflater().inflate(R.menu.overflow, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        if(menuItem.getTitle().equals("Grades")){
                            Intent intent = new Intent(MainActivity.this, GradesActivity.class);
                            startActivity(intent);
                            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                        } else if(menuItem.getTitle().equals("Logout")){
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                android.webkit.CookieManager.getInstance().removeAllCookies(null);
                            }
                            else {
                                android.webkit.CookieManager.getInstance().removeAllCookie();
                            }
                            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                            intent.putExtra("Logout", 1);
                            finish();
                            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                            startActivity(intent);
                            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                        } else {
                            SharedPreferences.Editor editor = getSharedPreferences("Theme", MODE_PRIVATE).edit();
                            if (menuItem.getTitle().equals("Aubergine")) {
                                editor.putInt("Color1", -5634197);
                                editor.putInt("Color2", -10419105);
                            } else if (menuItem.getTitle().equals("Cherry")) {
                                editor.putInt("Color1", -1363127);
                                editor.putInt("Color2", -762813);
                            } else if (menuItem.getTitle().equals("Mango Pulp")) {
                                editor.putInt("Color1", -1009639);
                                editor.putInt("Color2", -1188259);
                            } else if (menuItem.getTitle().equals("Green Beach")) {
                                editor.putInt("Color1", -16602448);
                                editor.putInt("Color2", -16724564);
                            } else if (menuItem.getTitle().equals("Mantle")) {
                                editor.putInt("Color1", -14367012);
                                editor.putInt("Color2", -11449699);
                            }
                            editor.apply();
                            setTheme();
                        }
                        return true;
                    }
                });
                popupMenu.show();
            }
        });
    }

    void setTheme(){
        SharedPreferences preferences = getSharedPreferences("Theme", MODE_PRIVATE);
        LinearLayout mainLayout = findViewById(R.id.main_layout);
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM,
                new int[] {preferences.getInt("Color1", -14367012), preferences.getInt("Color2", -11449699)});
        gd.setCornerRadius(0f);
        mainLayout.setBackground(gd);
    }

    public class setImage extends AsyncTask<Void, Void, Boolean>{

        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        URL myUrl = null;
        URLConnection urlConn = null;
        Bitmap bmp = null;

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                myUrl = new URL(propic);
                urlConn = myUrl.openConnection();
                String cookies = "";
                for(Map.Entry<String,String> entry:cookie.entrySet()){
                    cookies = cookies + entry + "; ";
                }
                urlConn.setRequestProperty("Cookie", cookies);
                bmp = BitmapFactory.decodeStream(urlConn.getInputStream());
                return true;

            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }


        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if(aBoolean){
                RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), bmp);
                circularBitmapDrawable.setCircular(true);
                imageView.setImageDrawable(circularBitmapDrawable);
            }
        }

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        setTheme();
    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Press back again to leave.", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

}
