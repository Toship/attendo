package com.nCubes.Attendo;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.nCubes.Attendo.Data.AttendoContract.Result;
import com.nCubes.Attendo.Data.ResultsDBHelper;
import com.nCubes.Attendo.Glearn.Grades;
import com.nCubes.Attendo.Glearn.RankRetiver;
import com.victor.loading.rotate.RotateLoading;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import fr.ganfra.materialspinner.MaterialSpinner;

/**
 * A login screen that offers login via email/password.
 */
public class GradesActivity extends AppCompatActivity {

    /**
     * Sqlite database object
     */
    private ResultsDBHelper DBHelper;
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    // UI references.
    private AutoCompleteTextView mEmailView;
    private MaterialSpinner spinner;
    private Spinner rankspinner;
    private RotateLoading mProgressView;
    private View mLoginFormView;
    private NestedScrollView resultPage;
    private LinearLayout loginPage;

    private String rollnumber;
    private String semester;
    private String SGPA;
    private String CGPA;

    private TextView showRank;
    private ProgressBar rankProgressBar;
    private Button rankButton;
    private RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grades);

        loginPage = findViewById(R.id.result_login_layout);
        resultPage = findViewById(R.id.result_page_layout);
        loginPage.setVisibility(View.VISIBLE);
        resultPage.setVisibility(View.GONE);

        mEmailView = findViewById(R.id.user_id);
        spinner = findViewById(R.id.sem_spinner);
        rankspinner = findViewById(R.id.your_rank_is);
        showRank = findViewById(R.id.result_rank);
        rankProgressBar = findViewById(R.id.rank_progress);
        recyclerView = findViewById(R.id.grades_recycle_view);
        mLoginFormView = findViewById(R.id.login_form_grades);
        mProgressView = findViewById(R.id.results_progress);

        TextView loginHeader = findViewById(R.id.results_page_attendo);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Milkshake.ttf");
        loginHeader.setTypeface(typeface);

        setLoginSpinnner();
        //Rank Spinner Setup
        ArrayAdapter<CharSequence> rankadapter = ArrayAdapter.createFromResource(this,
                R.array.rank_type, android.R.layout.simple_spinner_item);
        rankadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        rankspinner.setAdapter(rankadapter);

        //Setting OnClickListener On Login Button
        Button mEmailSignInButton = findViewById(R.id.view_result);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        //setting OnClickListener On Rank button
        rankButton = findViewById(R.id.rank_button);
        rankButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rankspinner == null || rankspinner.getSelectedItem() == null) {
                    Toast.makeText(GradesActivity.this, "Select Rank type!", Toast.LENGTH_SHORT).show();
                } else {
                    rankButton.setVisibility(View.GONE);
                    rankProgressBar.setVisibility(View.VISIBLE);
                    GetRank getRank = new GetRank(UpdateDB(rollnumber, semester, SGPA, CGPA));
                    getRank.execute();
                }
            }
        });
        rankspinner.setSelection(1);
        rankspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                rankButton.setVisibility(View.VISIBLE);
                rankProgressBar.setVisibility(View.GONE);
                showRank.setVisibility(View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(GradesActivity.this, "Select Rank type!", Toast.LENGTH_SHORT).show();
            }
        });

        setTheme();
    }

    void setTheme(){
        SharedPreferences preferences = getSharedPreferences("Theme", MODE_PRIVATE);
        RelativeLayout mainLayout = findViewById(R.id.main_layout_grades);
        NestedScrollView resultLayout = findViewById(R.id.result_page_layout);
        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM,
                new int[] {preferences.getInt("Color1", -14367012), preferences.getInt("Color2", -11449699)});
        gd.setCornerRadius(0f);
        mainLayout.setBackground(gd);
        resultLayout.setBackground(gd);
    }

    /**
     * Check if database already exists
     */
    private int UpdateDB(String roll, String sem, String sgpa, String cgpa){

        DBHelper = new ResultsDBHelper(GradesActivity.this);
        SQLiteDatabase db = DBHelper.getReadableDatabase();
        String query = "SELECT * FROM " + Result.TABLE_NAME + " WHERE " + Result._ID + " == \""
                + roll + "\" AND " + Result.RESULTS_SEM + " == " + sem + ";";
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.getCount() == 0){
            db.execSQL("DELETE FROM " + Result.TABLE_NAME);
            db.close();
            db = DBHelper.getReadableDatabase();
            ContentValues values = new ContentValues();
            values.put(Result._ID, roll);
            values.put(Result.RESULTS_SEM, sem);
            values.put(Result.RESULTS_SGPA, sgpa);
            values.put(Result.RESULTS_CGPA, cgpa);
            db.insert(Result.TABLE_NAME, null, values);
            cursor.close();
            db.close();
            DBHelper.close();
            //Toast.makeText(GradesActivity.this, "New Data!", Toast.LENGTH_SHORT).show();
            return 1;
        }
        else {
            cursor.moveToFirst();
            //System.out.println(cursor.getString(0) + "," + cursor.getFloat(1) + "," + cursor.getFloat(2));
            cursor.close();
            db.close();
            DBHelper.close();
            return 0;
        }
    }

    void setLoginSpinnner(){
        String[] sem = new String[15];
        for (Integer i = 1; i < 16; i++) {
            sem[i - 1] = i.toString();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, sem);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    /**
     * Hide Keyboard
     */
    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * Internet Connection Status
     */
    private boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        hideKeyboard();

        // Reset errors.
        mEmailView.setError(null);

        // Store values at the time of the login attempt.
        rollnumber = mEmailView.getText().toString();


        boolean cancel = false;
        View focusView = null;

        try {
            semester = spinner.getSelectedItem().toString();
        } catch (Exception e) {
            Toast.makeText(this, "Enter Semester!", Toast.LENGTH_SHORT).show();
            return;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(rollnumber)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        }
        try {
            Long.parseLong(rollnumber);
        } catch (Exception e) {
            mEmailView.setError("Invalid Username!");
            focusView = mEmailView;
            cancel = true;
        }

        if (!isConnected()) {
            Toast.makeText(this, "No Internet Connection!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mAuthTask = new UserLoginTask();
            mAuthTask.execute((Void) null);
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        mProgressView.start();
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE: View.GONE);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (loginPage.getVisibility() == View.VISIBLE)
            super.onBackPressed();
        else {
            loginPage.setVisibility(View.VISIBLE);
            resultPage.setVisibility(View.GONE);
        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Integer> {

        private final String url = "https://doeresults.gitam.edu/onlineresults/pages/Newgrdcrdinput1.aspx";
        private Document result;
        private ArrayList<Grades> grades = new ArrayList<>();

        UserLoginTask() {
        }

        private String StudentName() {
            return result.select("span[id=lblname]").text();
        }

        private String getSem(){
            return result.select("span[id=lblsem]").text();
        }

        private String getGPA() {
            SGPA = result.select("span[id=lblgpa]").text();
            return SGPA;
        }

        private String getCGPA() {
            CGPA = result.select("span[id=lblcgpa]").text();
            return CGPA;
        }

        private String getBranch() {
            return result.select("span[id=lblbranch]").text();
        }

        private String getCourse() {
            return result.select("span[id=lblcourse]").text() + " " + result.select("span[id=Label11]").text();
        }

        private void getResult() {
            org.jsoup.select.Elements realdata = result.select("table[class=table-responsive]").select("tr");
            for (Element sdata : realdata) {
                Elements element = sdata.select("th, td");
                //System.out.println(element);
                String CName = element.get(1).text();
                CName = CName.replace("ELECTIVE: ", "");
                String Credits = element.get(2).text();
                String Grade = element.get(3).text();
                grades.add(new Grades(CName, Credits, Grade));
            }
        }

        @Override
        protected Integer doInBackground(Void... params) {
            try {
                Connection.Response res = Jsoup.connect(url).method(Connection.Method.GET).execute();
                Document login = res.parse();
                org.jsoup.select.Elements inputs = login.select("input[name]");
                Map<String, String> inpdata = new HashMap<>();
                for (Element sinput : inputs) {
                    if (sinput.attr("name").equals("Button2") || sinput.attr("name").equals("txtreg"))
                        continue;
                    inpdata.put(sinput.attr("name"), sinput.attr("value"));
                }
                inpdata.put("txtreg", rollnumber);
                inpdata.put("cbosem", semester);
                Connection.Response res2 = Jsoup.connect(url).data(inpdata).cookies(res.cookies())
                        .method(Connection.Method.POST).execute();
                result = res2.parse();
                if (result.title().equals("DOE Results"))
                    return 0;
                else
                    return 1;
            } catch (IOException e) {
                e.printStackTrace();
                return 2;
            }

        }

        @Override
        protected void onPostExecute(final Integer status) {
            mAuthTask = null;
            showProgress(false);

            if (status == 0) {
                getResult();
                TextView SData = findViewById(R.id.result_name_textview);
                TextView CGPA = findViewById(R.id.result_cgpa_textview);
                TextView GPA = findViewById(R.id.result_gpa_textview);
                TextView CName = findViewById(R.id.textView);
                ImageView imageView = findViewById(R.id.imageView);

                imageView.setImageResource(R.drawable.gitamlogo);
                SData.setText(StudentName() + "\n" + rollnumber + "\n" + "Semester - "
                        + getSem() + "\n" + getBranch());
                CGPA.setText("CGPA : " + getCGPA());
                GPA.setText("SGPA : " + getGPA());
                CName.setText(getCourse());
                resultPage.setVisibility(View.VISIBLE);
                loginPage.setVisibility(View.GONE);

                /**
                 * Rank visibility
                 */
                showRank.setVisibility(View.GONE);
                rankProgressBar.setVisibility(View.GONE);
                rankButton.setVisibility(View.VISIBLE);

                recyclerView = findViewById(R.id.grades_recycle_view);
                recyclerView.setLayoutManager(new LinearLayoutManager(GradesActivity.this));
                AttendanceAdapter adapter = new AttendanceAdapter(grades, 4);
                recyclerView.setAdapter(adapter);

            } else if (status == 1) {
                mEmailView.setError("Invalid Information");
                mEmailView.requestFocus();
            } else if (status == 2) {
                AlertDialog alertDialog = new AlertDialog.Builder(GradesActivity.this).create();
                alertDialog.setTitle("Connection Error!");
                alertDialog.setMessage("Slow Internet Connection!");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                try {
                    alertDialog.show();
                } catch (Exception e) {
                    Toast.makeText(GradesActivity.this, "Slow Internet Connection!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }


        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }

    public class GetRank extends AsyncTask<Void, Void, Integer> {

        int DATABASE_NOT_AVAILABLE = 1;
        private int status;

        GetRank(int status) {
            this.status = status;
        }

        void selectSGPARank(){
            SQLiteDatabase db = DBHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("SELECT DISTINCT(" + Result.RESULTS_SGPA + ") FROM "
                    + Result.TABLE_NAME + " ORDER BY " + Result.RESULTS_SGPA + " DESC;",
                    null);
            String sqlsgpa;
            while(cursor.moveToNext()){
                sqlsgpa = cursor.getString(cursor.getColumnIndex(Result.RESULTS_SGPA));
                if(SGPA.equals(sqlsgpa)){

                    showRank.setVisibility(View.VISIBLE);
                    rankProgressBar.setVisibility(View.GONE);

                    showRank.setText("#" + (cursor.getPosition()+1));
                    //System.out.println(cursor.getPosition()+1);
                    break;
                }
            }
            cursor.close();
            db.close();
            DBHelper.close();
        }

        void selectCGPARank(){
            SQLiteDatabase db = DBHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("SELECT DISTINCT(" + Result.RESULTS_CGPA + ") FROM "
                            + Result.TABLE_NAME + " ORDER BY " + Result.RESULTS_CGPA + " DESC;",
                    null);
            String sqlcgpa;
            while(cursor.moveToNext()){
                sqlcgpa = cursor.getString(cursor.getColumnIndex(Result.RESULTS_CGPA));
                if(CGPA.equals(sqlcgpa)){

                    showRank.setVisibility(View.VISIBLE);
                    rankProgressBar.setVisibility(View.GONE);

                    showRank.setText("#" + (cursor.getPosition()+1));
                    //System.out.println(cursor.getPosition()+1);
                    break;
                }
            }
            cursor.close();
            db.close();
            DBHelper.close();
        }

        @Override
        protected Integer doInBackground(Void... voids) {

            if(status == DATABASE_NOT_AVAILABLE){
                RankRetiver.check = 0;
                String rollno = rollnumber.substring(0, rollnumber.length() -2);
                ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(8);
                DecimalFormat formatter = new DecimalFormat("00");
                for(int i=1; i<=70; i++){
                    String regNo = rollno + formatter.format(i);
                    if(regNo.equals(rollnumber)){
                        continue;
                    }
                    while (executor.getActiveCount() == 8) {
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        if (executor.getActiveCount() < 8)
                            break;
                    }
                    RankRetiver work = new RankRetiver(regNo, semester, GradesActivity.this);
                     if(RankRetiver.check > 5) {
                         break;
                     }
                    executor.execute(work);
                }
                executor.shutdown();
                while(!executor.isTerminated()){
                    try {
                        Thread.sleep(1000);
                        if (RankRetiver.error) {
                            executor.shutdownNow();
                            break;
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }

            return null;
        }

        private void showDialog(){
            AlertDialog alertDialog = new AlertDialog.Builder(GradesActivity.this).create();
            alertDialog.setTitle("Error");
            alertDialog.setMessage("Slow Internet Connection!");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            rankButton.setVisibility(View.VISIBLE);
                            rankProgressBar.setVisibility(View.GONE);
                            dialog.dismiss();
                        }
                    });
            try {
                alertDialog.show();
            } catch (Exception e) {
                Toast.makeText(GradesActivity.this, "Slow Internet Connection!", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }

        @Override
        protected void onPostExecute(Integer integer) {
            if (RankRetiver.error) {
                RankRetiver.error = false;
                showDialog();
                SQLiteDatabase db = DBHelper.getReadableDatabase();
                db.execSQL("DELETE FROM " + Result.TABLE_NAME);
                db.close();
            }
            int s = rankspinner.getSelectedItemPosition();
            switch (s){
                case 1:
                    selectSGPARank(); break;
                case 2:
                    selectCGPARank(); break;
            }
            DBHelper.close();
            super.onPostExecute(integer);
        }

    }
}

