package com.nCubes.Attendo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nCubes.Attendo.Glearn.Attendance;
import com.nCubes.Attendo.Glearn.Course;
import com.nCubes.Attendo.Glearn.Grades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by Toship on 16-09-2017.
 */

public class AttendanceAdapter extends RecyclerView.Adapter<AttendanceAdapter.MyViewHolder> {

    private final static int HORIZONTAL_CIRCULAR_PROGRESS = 1;
    private final static int ATTENDANCE_FILTER = 2;
    private final static int VERTICAL_ATTENDANCE = 3;
    private final static int VERTICAL_GRADE = 4;
    private static Map<String, String> cookie = null;
    private static int[] color1 = {-16732005, -243174, -14499389, -26266, -8453889, -16335178,
            -10212493, -16777216, -3459733, -14140282};
    private static int[] color2 = {-6895299, -542925, -148691, -41374, -2031361, -12013121,
            -3784082, -15754481, -4374734, -12213689};
    private static Integer[] colorOrder = null;
    private ArrayList<Course> courses = null;
    private ArrayList<Attendance> attendances = null;
    private ArrayList<Grades> grades = null;
    private String[] courseName = null;
    private Boolean[] status = null;
    private int orientation;
    private  Context context = null;

    //Circular Progress Adapter
    AttendanceAdapter(ArrayList<Course> courses, int orientation, Map<String, String> cookie,
                      Integer[] colorOrder, Context context){
        this.courses = courses;
        this.context = context;
        this.orientation = orientation;
        AttendanceAdapter.cookie = cookie;
        AttendanceAdapter.colorOrder = colorOrder;
    }

    //Selective Attendance
    AttendanceAdapter(String[] courseName, Boolean[] status,  int orientation){
        this.courseName = courseName;
        this.status = status;
        this.orientation = orientation;
    }

    //Attendance Adapter
    AttendanceAdapter(int orientation, ArrayList<Attendance> attendances){
        this.attendances = attendances;
        this.orientation = orientation;
    }

    //Grades Adapter
    AttendanceAdapter(ArrayList<Grades> grades, int orientation) {
        this.grades = grades;
        this.orientation = orientation;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        if(orientation == HORIZONTAL_CIRCULAR_PROGRESS){
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_items, parent, false);
        }
        else if(orientation == VERTICAL_ATTENDANCE){
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.attendance_item, parent, false);
        }
        else if(orientation == VERTICAL_GRADE) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.table_result, parent, false);
        }
        else if(orientation == ATTENDANCE_FILTER){
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.filter_item, parent, false);
        }

        return new MyViewHolder(view, orientation);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        if(orientation == HORIZONTAL_CIRCULAR_PROGRESS){
            holder.subPer.setText(courses.get(holder.getAdapterPosition()).getPerAtten()+"%");

            //Trimming Course name to get course ID
            String c = courses.get(holder.getAdapterPosition()).getCourseAbv();
            holder.courseName.setText(c);

            Typeface typeface = Typeface.createFromAsset(holder.courseName.getContext().getAssets(), "fonts/hp_simplified.ttf");
            holder.courseName.setTypeface(typeface);

            float classTaken = courses.get(holder.getAdapterPosition()).getClassTaken();
            float totalclass = courses.get(holder.getAdapterPosition()).getTotalClass();
            float subPer = courses.get(holder.getAdapterPosition()).getPerAtten();
            //Positive Prediction
            float PP = (classTaken+1)*100/(totalclass+1);
            holder.textView.setText("+" + Math.round((PP - subPer)*100.0)/100.0+"%");
            //Negetive Prediction
            float NP = (classTaken)*100/(totalclass+1);
            holder.date.setText("-" + Math.round((subPer - NP)*100.0)/100.0+"%");

            //Setting gradient on Course item
            GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM,
                    new int[] {color1[colorOrder[holder.getAdapterPosition()%10]],
                            color2[colorOrder[holder.getAdapterPosition()%10]]});
            gd.setCornerRadius(50f);
            holder.courseView.setBackground(gd);

            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Toast.makeText(v.getContext(), courses.get(holder.getAdapterPosition())
                            .getName(), Toast.LENGTH_SHORT).show();
                    return true;
                }
            });

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), Classroom.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("course", courses.get(holder.getAdapterPosition()));
                    bundle.putSerializable("cookie", (Serializable) cookie);
                    intent.putExtras(bundle);
                    intent.putExtra("pos", holder.textView.getText());
                    intent.putExtra("neg", holder.date.getText());
                    v.getContext().startActivity(intent);
                    ((Activity) context).overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                }
            });
        }
        else if(orientation == ATTENDANCE_FILTER){
            holder.courseName.setText(courseName[holder.getAdapterPosition()]);
            if(status[holder.getAdapterPosition()]){
                holder.filterCheckBox.setChecked(true);
            } else {
                holder.filterCheckBox.setChecked(false);
            }
            holder.filterCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    status[holder.getAdapterPosition()] = b;
                }
            });
            holder.courseName.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    Toast.makeText(view.getContext(), courseName[holder.getAdapterPosition()]
                            , Toast.LENGTH_SHORT).show();
                    return true;
                }
            });
        }
        else if(orientation == VERTICAL_ATTENDANCE){
            String status = attendances.get(holder.getAdapterPosition()).getStatus();
            holder.courseName.setText(attendances.get(holder.getAdapterPosition()).getTime());
            holder.date.setText(attendances.get(holder.getAdapterPosition()).getDate());
            holder.textView.setText("");
            if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP){
                if(Pattern.compile("Present").matcher(status).find())
                    holder.textView.setBackgroundColor(Color.parseColor("#00ff00"));
                else if(Pattern.compile("Absent").matcher(status).find())
                    holder.textView.setBackgroundColor(Color.parseColor("#ff0000"));
                else if(Pattern.compile("Late").matcher(status).find())
                    holder.textView.setBackgroundColor(Color.parseColor("#ECB500"));
                else {
                    holder.textView.setBackgroundColor(Color.parseColor("#000000"));
                    holder.textView.setText("?");
                }
            }
            else {
                if(Pattern.compile("Present").matcher(status).find())
                    holder.textView.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#00ff00")));
                else if(Pattern.compile("Absent").matcher(status).find())
                    holder.textView.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#ff0000")));
                else if(Pattern.compile("Late").matcher(status).find())
                    holder.textView.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#ECB500")));
                else {
                    holder.textView.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#000000")));
                    holder.textView.setText("?");
                }
            }
        }
        else if (orientation == VERTICAL_GRADE) {
            if(holder.getAdapterPosition() == 0){
                holder.courseName.setTypeface(null, Typeface.BOLD);
                holder.date.setTypeface(null, Typeface.BOLD);
                holder.textView.setTypeface(null, Typeface.BOLD);
            } else {
                holder.courseName.setTypeface(null, Typeface.NORMAL);
                holder.date.setTypeface(null, Typeface.NORMAL);
                holder.textView.setTypeface(null, Typeface.NORMAL);
            }
            holder.courseName.setText(grades.get(holder.getAdapterPosition()).getCName());
            holder.date.setText(grades.get(holder.getAdapterPosition()).getCredits());
            holder.textView.setText(grades.get(holder.getAdapterPosition()).getGrades());
        }
    }

    @Override
    public int getItemCount() {
        if(orientation == VERTICAL_ATTENDANCE)
            return attendances.size();
        else if(orientation == VERTICAL_GRADE)
            return grades.size();
        else if(orientation == ATTENDANCE_FILTER)
            return courseName.length;
        return courses.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder{

        TextView subPer;
        TextView textView;
        TextView courseName;
        TextView date;
        CheckBox filterCheckBox;
        LinearLayout courseView;
        int orientation;

        MyViewHolder(View itemView, int orientation) {
            super(itemView);
            this.orientation = orientation;
            if(orientation == HORIZONTAL_CIRCULAR_PROGRESS){
                subPer = itemView.findViewById(R.id.atten_per_sub);
                courseName = itemView.findViewById(R.id.sub_name);
                textView = itemView.findViewById(R.id.atten_plus);
                date = itemView.findViewById(R.id.atten_minus);
                courseView = itemView.findViewById(R.id.relativeLayout);
            }
            else if(orientation == ATTENDANCE_FILTER){
                courseName = itemView.findViewById(R.id.filter_subject);
                filterCheckBox = itemView.findViewById(R.id.filter_check_box);
            }
            else if(orientation == VERTICAL_ATTENDANCE) {
                textView = itemView.findViewById(R.id.status);
                courseName = itemView.findViewById(R.id.clstime);
                date = itemView.findViewById(R.id.date);
            }
            else if(orientation == VERTICAL_GRADE){
                courseName = itemView.findViewById(R.id.result_subject);
                date = itemView.findViewById(R.id.result_credits);
                textView = itemView.findViewById(R.id.result_grade);
            }
        }

    }
}
