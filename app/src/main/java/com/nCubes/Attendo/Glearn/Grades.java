package com.nCubes.Attendo.Glearn;

/**
 * Created by Toship on 23-09-2017.
 */

public class Grades {

    private String CName;
    private String Credits;
    private String Grades;

    public Grades(String CName, String Credits, String Grades){
        this.CName = CName;
        this.Credits = Credits;
        this.Grades = Grades;
    }

    public String getCName(){
        return CName;
    }

    public String getCredits(){
        return Credits;
    }

    public String getGrades(){
        return Grades;
    }

}
