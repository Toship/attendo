package com.nCubes.Attendo.Glearn;

import java.io.Serializable;
import java.util.LinkedHashMap;

/**
 * Created by Toship on 21-09-2017.
 */

public class Resource implements Serializable {

    private LinkedHashMap<String, LinkedHashMap<String, String>> resource;

    Resource(LinkedHashMap<String, LinkedHashMap<String, String>> resource ){
        this.resource = resource;
    }

    public LinkedHashMap<String, LinkedHashMap<String, String>> getResource(){
        return resource;
    }

}
