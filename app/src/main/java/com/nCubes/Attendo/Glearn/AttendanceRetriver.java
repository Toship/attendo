package com.nCubes.Attendo.Glearn;

import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Toship on 14-09-2017.
 */

class AttendanceRetriver {

    private String attendanceUrl;
    private Map<String, String> cookie;
    private Float attainper;
    private float totalClass;
    private float classTaken;
    private ArrayList<Attendance> attainList = new ArrayList<>();
    private boolean gotAttain;

    AttendanceRetriver(String link, Map<String, String> cookie){
        attendanceUrl = link + "&view=5";
        this.cookie = cookie;
    }

    void run() {

        try {
            Document attainPage = Jsoup.connect(attendanceUrl).cookies(cookie).get();
            Elements table = attainPage.select("table[class=generaltable attwidth boxaligncenter]")
                    .select("tbody").select("tr");
            String clsDetails[] = attainPage.select("table[class=attlist]")
                    .select("tr[class=normal]").get(1).select("td[class=cell c1 lastcol]")
                    .text().split(" / ");
            classTaken = Float.parseFloat(clsDetails[0]);
            totalClass = Float.parseFloat(clsDetails[1]);
            String per = attainPage.select("table[class=attlist]")
                    .select("tr[class=normal]").get(2).select("td[class=cell c1 lastcol]")
                    .text();
            attainper = Float.parseFloat(per.substring(0, per.length() - 1));
            for(Element row:table){
                String date = row.select("td[class=cell c2]").text();
                String time = row.select("td[class=cell c3]").text();
                String status = row.select("td[class=cell c5]").text();
                attainList.add(new Attendance(date, time, status));
            }
            gotAttain = true;
        } catch (IOException e) {
            Log.e("AttendanceRetriver", "IOException", e);
            gotAttain = false;
        } catch (NullPointerException e) {
            Log.e("AttendanceRetriver", "NullPointerException", e);
            gotAttain = false;
        } catch (IndexOutOfBoundsException e) {
            Log.e("AttendanceRetriver", "IndexOutOfBound on Thread Stop", e);
            gotAttain = false;
        }

    }

    Float getAttainper(){
        if(gotAttain)
            return attainper;
        else
            return null;
    }

    Float getTotalClass(){
        if(gotAttain)
            return totalClass;
        else
            return null;
    }

    Float getClassTaken(){
        if(gotAttain)
            return classTaken;
        else
            return null;
    }

    ArrayList<Attendance> getAttainList(){
        //System.out.println("Attendance Retriver getAttainList " + gotAttain);
        if(gotAttain)
            return attainList;
        else
            return null;
    }

}
