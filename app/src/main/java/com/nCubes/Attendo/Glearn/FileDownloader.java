package com.nCubes.Attendo.Glearn;

import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.widget.Toast;

import com.nCubes.Attendo.CourseContent;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by Toship on 21-09-2017.
 */

public class FileDownloader extends AsyncTask<Void, Void, Boolean> {

    private String Url;
    private String FileName;
    @SuppressLint("StaticFieldLeak")
    private Context context;
    private Map<String, String> cookie;
    private Resource resource;
    private int mode;
    private String CourseName;
    private float attainper;

    public FileDownloader(Context context, String FileName, String Url, Map<String, String> cookie,
                          String CourseName, float attainper){
        this.context = context;
        this.FileName = FileName;
        this.Url = Url;
        this.cookie = cookie;
        this.CourseName = CourseName;
        this.attainper = attainper;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(Pattern.compile("\\/folder\\/").matcher(Url).find()){
            mode = 1;
        }
        else if(Pattern.compile("\\/resource\\/").matcher(Url).find()) {
            mode = 2;
        }
        else {
            mode = 3;
        }
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        if(mode == 1){
            try {
                LinkedHashMap<String, LinkedHashMap<String, String>> resourses = new
                        LinkedHashMap<>();
                LinkedHashMap<String, String> a = new LinkedHashMap<>();
                Document page = Jsoup.connect(Url).cookies(cookie).get();
                Elements filename = page.select("span[class=fp-filename-icon]");
                for(Element element:filename) {
                    String link = element.select("a").attr("href");
                    String name = element.text();
                    a.put(name, link);
                }
                resourses.put(page.select("h2").first().text(), a);
                System.out.println(resourses);
                resource = new Resource(resourses);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
        else if(mode == 2){
            try {
                Connection.Response file = Jsoup.connect(Url).cookies(cookie).ignoreContentType(true)
                        .followRedirects(true).execute();
                String[] e = file.header("Content-Disposition").split("\"");
                FileName = e[e.length -1];
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }

    @Override
    protected void onPostExecute(Boolean status) {
        super.onPostExecute(status);
        if(!status){
            Toast.makeText(context, "Slow Internet Connection!\nTry again!", Toast.LENGTH_SHORT).show();
            return;
        }
        if(mode == 1) {
            Intent intent = new Intent(context, CourseContent.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("resource", resource);
            bundle.putSerializable("cookie", (Serializable) cookie);
            intent.putExtra("attain", attainper);
            intent.putExtra("name", CourseName);
            intent.putExtras(bundle);
            context.startActivity(intent);
        }
        else {
            Uri uri = Uri.parse(Url);
            String cookies = "MoodleSession="+cookie.get("MoodleSession");
            //System.out.println(cookies);
            try {
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).mkdir();
                DownloadManager.Request request = new DownloadManager.Request(uri)
                        .addRequestHeader("Cookie", cookies)
                        .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, FileName)
                        .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                downloadManager.enqueue(request);
            } catch (Exception e){
                Toast.makeText(context, "Unable To download. Report to developer!", Toast.LENGTH_LONG).show();
            }
        }
    }
}
