package com.nCubes.Attendo.Glearn;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Toship on 14-09-2017.
 */

public class Course implements Serializable{

    private String courseName = null;
    private String CourseAbv = "";
    private String[] CourseId = null;
    private ArrayList<Attendance> attendance = new ArrayList<>();
    private Float perAttendance = null;
    private Float totalClass = null;
    private Float classTaken = null;
    private Resource resources;

    public Course(String courseName, Float perAttendance, Float totalClass, Float classTaken,
                  ArrayList<Attendance> attendance, Resource resources){
        this.courseName = courseName;
        this.perAttendance = perAttendance;
        this.totalClass = totalClass;
        this.classTaken = classTaken;
        this.attendance = attendance;
        this.resources = resources;
        this.CourseId = courseName.split(" ");
        setCourseAbv();
    }

    private void setCourseAbv() {
        for(int i = 2; i < CourseId.length; i++){
            if (CourseId[i].toUpperCase().equals("AND") || CourseId[i].substring(0, 1).equals("-"))
                continue;
            CourseAbv = CourseAbv + CourseId[i].substring(0,1);
        }
    }

    public String getName(){
        return this.courseName;
    }

    public ArrayList<Attendance> getAttendance(){
        return this.attendance;
    }

    public Float getPerAtten(){
        return this.perAttendance;
    }

    public Float getTotalClass(){
        return this.totalClass;
    }

    public Float getClassTaken(){
        return this.classTaken;
    }

    public Resource getResources() {
        return resources;
    }

    public String getCourseId(){
        return CourseId[0];
    }

    public String getCourseAbv(){
        return CourseAbv;
    }

}
