package com.nCubes.Attendo.Glearn;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.nCubes.Attendo.Data.AttendoContract.Result;
import com.nCubes.Attendo.Data.ResultsDBHelper;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Toship on 18-10-2017.
 */

public class RankRetiver implements Runnable {

    private static String url = "https://doeresults.gitam.edu/onlineresults/pages/Newgrdcrdinput1.aspx";
    private String roll;
    private String sem;
    private Context context;

    public static int check = 0;
    public static boolean error = false;

    public RankRetiver(String roll, String sem, Context context){
        this.roll = roll;
        this.sem = sem;
        this.context = context;
    }

    synchronized private void insert(String sgpa, String cgpa){
        ResultsDBHelper DBHelper = new ResultsDBHelper(context);
        SQLiteDatabase database = DBHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Result._ID, roll);
        values.put(Result.RESULTS_SEM, sem);
        values.put(Result.RESULTS_SGPA, sgpa);
        values.put(Result.RESULTS_CGPA, cgpa);
        database.insert(Result.TABLE_NAME, null, values);
        database.close();
        DBHelper.close();
    }

    @Override
    public void run() {

        try {
            Connection.Response res = Jsoup.connect(url).method(Connection.Method.GET).execute();
            Document login = res.parse();
            org.jsoup.select.Elements inputs = login.select("input[name]");
            Map<String, String> inpdata = new HashMap<>();
            for(Element sinput:inputs){
                if(sinput.attr("name").equals("Button2") || sinput.attr("name").equals("txtreg"))
                    continue;
                inpdata.put(sinput.attr("name"), sinput.attr("value"));
            }
            inpdata.put("txtreg", roll);
            inpdata.put("cbosem", sem);
            Connection.Response res2 = Jsoup.connect(url).data(inpdata).cookies(res.cookies())
                    .method(Connection.Method.POST).execute();
            Document result = res2.parse();
            if(result.title().equals("DOE Results"))
                insert(result.select("span[id=lblgpa]").text(), result.select("span[id=lblcgpa]").text());
            else {
                synchronized (this) {
                    RankRetiver.check++;
                }
            }

        } catch (Exception ex) {
            error = true;
            Logger.getLogger(RankRetiver.class.getName()).log(Level.SEVERE, "null", ex);
        }

    }
}
