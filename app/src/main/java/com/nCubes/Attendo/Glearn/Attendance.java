package com.nCubes.Attendo.Glearn;

import java.io.Serializable;

/**
 * Created by Toship on 14-09-2017.
 */

public class Attendance implements Serializable{

    private String time;
    private String status;
    private String date;

    Attendance(String date, String time, String status){
        this.date = date;
        this.time = time;
        this.status = status;
    }

    public String getTime(){
        return this.time;
    }

    public String getDate(){
        return this.date;
    }

    public String getStatus(){
        return this.status;
    }

}
