package com.nCubes.Attendo.Glearn;

import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Toship on 14-09-2017.
 */

public class CourseRetriver implements Runnable {

    private String mCourseUrl;
    private Map<String, String> cookie;
    private Course course = null;
    private boolean gotEverything;

    public CourseRetriver(String courseUrl, Map<String, String> cookie){
        this.mCourseUrl = courseUrl;
        this.cookie = cookie;
    }

    @Override
    public void run() {
        try {
            String attainUrl;
            Document coursePage = Jsoup.connect(mCourseUrl).cookies(cookie).get();
            Element link = coursePage.select("li[class=activity attendance modtype_attendance]")
                    .select("a").first();
            attainUrl = link.attr("href");
            AttendanceRetriver attainretrive = new AttendanceRetriver(attainUrl, cookie);
            attainretrive.run();
            LinkedHashMap<String, LinkedHashMap<String, String>> resources =
                    new LinkedHashMap<>();
            int i = 0;
            while(true){
                String name;
                String reslink;
                String heading;
                LinkedHashMap<String, String> resource = new LinkedHashMap<>();
                Elements crsres = coursePage.select("li[id=section-"+i+"]");
                Elements rows = crsres.select("li[class=activity folder modtype_folder], li[class=activity resource modtype_resource]");
                if(rows.isEmpty())
                    break;
                heading = crsres.select("h3").text();
                //System.out.println(heading);
                for(Element one:rows){
                    Elements filenames = one.select("li").select("span[class=fp-filename-icon]");
                    if(!filenames.select("span[class=fp-filename]").isEmpty()){
                        for(Element s:filenames){
                            name = s.text();
                            reslink = s.select("a").attr("href");
                            resource.put(name, reslink);
                            //System.out.println(name+" - "+reslink);
                        }
                    }
                    else{
                        name = one.text().replace(" File", "").replace(" Folder", "");
                        reslink = one.select("a").attr("href");
                        resource.put(name, reslink);
                    }
                }
                resources.put(heading, resource);
                i++;

            }
            //Removing unnecssary bracketed String of Course name
            String courseName = coursePage.select("h1").first().text();
            Pattern P = Pattern.compile("\\([A-Z]*\\)|\\([A-Za-z\\s]*Semester?.*\\)|\\([A-Za-z\\s]*section?.*\\)");
            Matcher m = P.matcher(courseName);
            courseName = m.replaceAll("");

            course = new Course(courseName, attainretrive.getAttainper(), attainretrive.getTotalClass(),
                    attainretrive.getClassTaken(), attainretrive.getAttainList(), new Resource(resources));
            gotEverything = true;

            System.out.println(Thread.currentThread().getName() + courseName);

        } catch (IOException e) {
            Log.e("CourseRetriver", "IOException", e);
            gotEverything = false;
        } catch (NullPointerException e){
            Log.e("CourseRetriver", "NullPointerException", e);
            gotEverything = false;
        }
    }

    public Course getCourse(){

        if(gotEverything)
            return course;
        else
            return null;
    }

}
